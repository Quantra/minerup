# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-19 14:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miners', '0013_auto_20170715_0258'),
    ]

    operations = [
        migrations.AddField(
            model_name='worker',
            name='last_hashrate',
            field=models.FloatField(default=0),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-12 14:01
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pools', '0014_auto_20170707_1921'),
        ('miners', '0008_worker_disabled'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='wallet',
            unique_together=set([('user', 'address', 'coin')]),
        ),
    ]

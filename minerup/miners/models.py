from django.db import models
from django.utils import timezone

from minerup.models_base import AutoDateModel
from pools.models import Pool, Coin
from usercustom.models import User


class Wallet(AutoDateModel):
    user = models.ForeignKey(User, related_name='wallets')
    address = models.CharField(max_length=64)
    email = models.EmailField(blank=True, null=True)
    coin = models.ForeignKey(Coin, related_name='wallets')

    class Meta:
        unique_together = ('user', 'address', 'coin')

    def __str__(self):
        return self.address


class Worker(AutoDateModel):
    OFFLINE = 10
    # FAILING = 20
    ONLINE = 30
    STATUS_CHOICES = (
        (OFFLINE, 'Offline'),
        # (FAILING, 'Failing'),
        (ONLINE, 'Online')
    )

    name = models.CharField(max_length=64)
    user = models.ForeignKey(User, related_name='workers')

    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=ONLINE)
    last_tested = models.DateTimeField(blank=True, null=True, default=timezone.now)

    up_since = models.DateTimeField(default=timezone.now)
    failure_started = models.DateTimeField(blank=True, null=True)

    last_pool = models.ForeignKey(Pool, blank=True, null=True, related_name='current_workers')
    last_wallet = models.ForeignKey(Wallet, blank=True, null=True, related_name='current_wallets')

    last_hashrate = models.FloatField(default=0)
    # last_balance = models.FloatField(default=0)

    disabled = models.BooleanField(default=False)

    class Meta:
        unique_together = ('name', 'user')

    def __str__(self):
        return self.name
    

class WorkerResult(models.Model):
    date = models.DateTimeField()
    worker = models.ForeignKey(Worker, related_name='worker_results')
    status = models.PositiveSmallIntegerField(choices=Worker.STATUS_CHOICES)
    hashrate = models.FloatField()
    pool = models.ForeignKey(Pool, related_name='worker_results')
    coin = models.ForeignKey(Coin, related_name='worker_results')
    # balance = models.FloatField(default=0)

    class Meta:
        unique_together = ('date', 'worker')
        ordering = ['date', ]


class ArchivedWorkerResult(models.Model):
    up_time = models.FloatField()
    down_time = models.FloatField()
    hashrate_min = models.FloatField()
    hashrate_max = models.FloatField()
    hashrate_avg = models.FloatField()

    class Meta:
        abstract = True
        ordering = ['date', ]
        unique_together = ('worker', 'date')


class PoolTimeModel(models.Model):
    pool = models.ForeignKey(Pool)
    time = models.FloatField()

    class Meta:
        abstract = True
        unique_together = ('result', 'pool')


class CoinTimeModel(models.Model):
    coin = models.ForeignKey(Coin)
    time = models.FloatField()

    class Meta:
        abstract = True
        unique_together = ('result', 'coin')


# class WorkerBalanceModel(models.Model):
#     coin = models.ForeignKey(Coin)
#     balance = models.FloatField()
#
#     class Meta:
#         abstract = True
#         unique_together = ('result', 'coin')


class HourlyWorkerResult(ArchivedWorkerResult):
    date = models.DateTimeField()
    worker = models.ForeignKey(Worker, related_name='hourly_results')


class HourlyPoolTime(PoolTimeModel):
    result = models.ForeignKey(HourlyWorkerResult, related_name='pool_time_results')


class HourlyCoinTime(CoinTimeModel):
    result = models.ForeignKey(HourlyWorkerResult, related_name='coin_time_results')


# class HourlyWorkerBalance(WorkerBalanceModel):
#     result = models.ForeignKey(HourlyWorkerResult, related_name='balances')


class DailyWorkerResult(ArchivedWorkerResult):
    date = models.DateField()
    worker = models.ForeignKey(Worker, related_name='daily_results')


class DailyPoolTime(PoolTimeModel):
    result = models.ForeignKey(DailyWorkerResult, related_name='pool_time_results')


class DailyCoinTime(CoinTimeModel):
    result = models.ForeignKey(DailyWorkerResult, related_name='coin_time_results')


# class DailyWorkerBalance(WorkerBalanceModel):
#     result = models.ForeignKey(DailyWorkerResult, related_name='balances')


class WeeklyWorkerResult(ArchivedWorkerResult):
    date = models.DateField()
    worker = models.ForeignKey(Worker, related_name='weekly_results')


class WeeklyPoolTime(PoolTimeModel):
    result = models.ForeignKey(WeeklyWorkerResult, related_name='pool_time_results')


class WeeklyCoinTime(CoinTimeModel):
    result = models.ForeignKey(WeeklyWorkerResult, related_name='coin_time_results')


# class WeeklyWorkerBalance(WorkerBalanceModel):
#     result = models.ForeignKey(WeeklyWorkerResult, related_name='balances')


class MonthlyWorkerResult(ArchivedWorkerResult):
    date = models.DateField()
    worker = models.ForeignKey(Worker, related_name='monthly_results')


class MonthlyPoolTime(PoolTimeModel):
    result = models.ForeignKey(MonthlyWorkerResult, related_name='pool_time_results')


class MonthlyCoinTime(CoinTimeModel):
    result = models.ForeignKey(MonthlyWorkerResult, related_name='pool_coin_results')

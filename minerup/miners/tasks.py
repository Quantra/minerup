from __future__ import absolute_import

import datetime

from celery import shared_task, uuid
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.cache import caches
from django.db.models import Sum, Min, Avg, Max
from django.template.loader import render_to_string
from django.utils import timezone
from slacker import Slacker

from miners.models import Worker, WorkerResult, HourlyWorkerResult, HourlyPoolTime, HourlyCoinTime, DailyWorkerResult, \
    DailyPoolTime, DailyCoinTime, WeeklyWorkerResult, WeeklyPoolTime, WeeklyCoinTime, MonthlyWorkerResult, \
    MonthlyPoolTime, MonthlyCoinTime
from pools.models import ApiEndpoint, ApiFunction
from usercustom.models import User

slack = Slacker(settings.SLACK_TOKEN)

logger = get_task_logger(__name__)

cache = caches['default']
mcache = caches['memcache']

TEST_RATE = 5 * 60


@shared_task(name='alert_user')
def alert_user(user_id, subject, msg):
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return

    if user.slack_alerts and user.slack_id:
        # todo handle errors such as requests.exceptions.ReadTimeout
        slack.chat.post_message(user.slack_id, msg, as_user=True)

    if user.email_alerts and user.email:
        user.email_user(subject, msg)


@shared_task(name='test_all_workers')
def test_all_workers():
    """
    Sentinel task for test_workers.
    :return:
    """
    for w_id in Worker.objects.filter(disabled=False).values_list('id', flat=True):
        test_worker.delay(w_id)


@shared_task(name='test_worker')
def test_worker(worker_id):
    """
    Given a worker searches through viable pools and wallet combos until it finds data via api functions.
    If no data can be found the worker is marked down and users alerted.
    :return:
    """
    w_cache_key = 'workertesttask' + str(worker_id)
    next_task_id = uuid()
    if not mcache.add(w_cache_key, next_task_id, TEST_RATE):
        if mcache.get(w_cache_key) != test_worker.request.id:
            logger.info("Task locked {}".format(worker_id))
            return
        mcache.set(w_cache_key, next_task_id, TEST_RATE)

    # Test the worker
    try:
        worker = Worker.objects.get(id=worker_id)
        logger.info('Test started for {} {}'.format(worker.id, worker.name))
    except Worker.DoesNotExist:
        logger.info('No worker found with id {}.  It was probably deleted.'.format(worker_id))
        return

    if worker.disabled:
        logger.info('{} {} has been disabled.'.format(worker.id, worker.name))
        return

    now = timezone.now()

    # build list of pools/wallets with last pool/wallet at top if there is one
    pools = worker.user.pools.all().prefetch_related('coins')
    if worker.last_pool:
        pools = [worker.last_pool] + list(pools.exclude(id=worker.last_pool_id))

    failed = True
    hashrate = 0
    for p in pools:
        if not failed:
            break
        logger.info('Looking for {} on {}'.format(worker, p))
        try:
            endpoint = ApiEndpoint.objects.get(id__in=p.endpoints.values_list('id', flat=True),
                                               endpoint_type=ApiEndpoint.WORKER_INFO)
        except ApiEndpoint.DoesNotExist:
            logger.info('ApiEndPoint DoesNotExist for WORKER_INFO {}'.format(p))
            continue

        try:
            test_worker_function = ApiFunction.objects.get(endpoint=endpoint, api_function=ApiFunction.TEST_WORKER)
        except ApiFunction.DoesNotExist:
            logger.info('ApiFunction DoesNotExist for TEST_WORKER {}'.format(p))
            test_worker_function = None

        try:
            hashrate_worker = ApiFunction.objects.get(endpoint=endpoint, api_function=ApiFunction.HASHRATE_WORKER)
        except ApiFunction.DoesNotExist:
            logger.info('ApiFunction DoesNotExist for HASHRATE_WORKER {}'.format(p))
            hashrate_worker = None

        wallets = worker.user.wallets.filter(coin__in=p.coins.values_list('id', flat=True))
        if worker.last_wallet and worker.last_wallet in wallets:
            wallets = [worker.last_wallet] + list(wallets.exclude(id=worker.last_wallet_id))

        for w in wallets:
            logger.info('Trying wallet {} for {} on {}'.format(w, worker, p))

            if failed and test_worker_function is not None:
                # Test worker is alive
                result = test_worker_function.exec_function(p.url, worker.name, w.address, email=w.email)

                if result:
                    failed = False

                    if worker.status != Worker.ONLINE:
                        # OFFLINE => ONLINE
                        worker.up_since = now
                        alert_user.delay(worker.user_id, 'Minerup - {} online'.format(worker.name),
                                         render_to_string('alerts/worker_online.html', {'worker': worker,
                                                                                        'pool': p,
                                                                                        'wallet': w}))

                    logger.info('Test passed for {} {}'.format(worker.id, worker.name))

                    worker.status = worker.ONLINE
                    worker.last_wallet = w
                    worker.last_pool = p

            if hashrate_worker is not None:
                hashrate += hashrate_worker.exec_function(p.url, worker.name, w.address, email=w.email,
                                                          hashrate_mod=p.hashrate_mod)

    if failed:
        if worker.status != Worker.OFFLINE:
            # ONLINE => OFFLINE
            worker.failure_started = now
            alert_user.delay(worker.user_id, 'Minerup - {} OFFLINE'.format(worker.name),
                             render_to_string('alerts/worker_offline.html', {'worker': worker}))

        logger.info('Test failed for {} {}'.format(worker.id, worker.name))

        worker.status = worker.OFFLINE

    # Queue next test
    test_worker.apply_async((worker_id,), countdown=TEST_RATE, task_id=next_task_id)

    worker.last_hashrate = hashrate
    # todo check this against the worker's alert hashrate and alert if needed use some bools to prevent spam

    worker_result = WorkerResult(date=now, worker_id=worker_id, status=worker.status, hashrate=hashrate,
                                 pool_id=worker.last_pool_id, coin_id=worker.last_wallet.coin_id)
    worker_result.save()

    worker.last_tested = now
    worker.save()


def calc_uptime(results):
    """
    Takes queryset dict of date and status.  Returns total uptime and total downtime in seconds.
    :param results:
    :return:
    """
    last_date = results[0]['date'].replace(minute=0, second=0, microsecond=0)
    final_date = last_date + timezone.timedelta(hours=1)
    last_status = results[0]['status']
    up_time = 0.0
    down_time = 0.0
    for result in results:
        if result['status'] == last_status:
            continue

        period = (result['date'] - last_date).total_seconds()
        if last_status == Worker.ONLINE:
            up_time += period
        elif last_status == Worker.OFFLINE:
            down_time += period

        last_status = result['status']
        last_date = result['date']

    period = (final_date - last_date).total_seconds()
    if last_status == Worker.OFFLINE:
        down_time += period
    else:
        up_time += period

    return up_time, down_time


def calc_times(results, id_field):
    last_date = results[0]['date'].replace(minute=0, second=0, microsecond=0)
    final_date = last_date + timezone.timedelta(hours=1)
    last_id = results[0][id_field]
    times = {}
    for result in results:
        if result[id_field] == last_id:
            continue

        period = (result['date'] - last_date).total_seconds()
        times[result[id_field]] = times.get(result[id_field], 0) + period

        last_id = result[id_field]
        last_date = result['date']

    period = (final_date - last_date).total_seconds()
    times[result[id_field]] = times.get(result[id_field], 0) + period

    return times


@shared_task(name='archive_all_hourly')
def archive_all_hourly():
    """
    Should run at the start of every hour.
    Loops over all workers and produces hourly results in separate tasks.
    Looks back and creates missing reports.
    :return:
    """
    one_hour = timezone.timedelta(hours=1)
    _start = timezone.now().replace(minute=0, second=0, microsecond=0) - one_hour
    for worker in Worker.objects.filter(disabled=False):
        start = _start
        while start >= worker.create_date and not HourlyWorkerResult.objects.filter(worker=worker, date=start).exists():
            archive_worker_hourly.delay(worker.id, start, start + one_hour)
            start -= one_hour


@shared_task(name='archive_worker_hourly')
def archive_worker_hourly(worker_id, start, end):
    """
    Creates hourly report per worker
    :param worker_id:
    :param start:
    :param end:
    :return:
    """
    worker_results = WorkerResult.objects.filter(worker_id=worker_id, date__gte=start, date__lt=end)
    if not worker_results.exists():
        logger.info('No worker results {}'.format(worker_id))
        return

    up_time, down_time = calc_uptime(worker_results.values('date', 'status'))

    hashrates = worker_results.values_list('hashrate', flat=True)

    hashrate_min = hashrate_max = hashrate_avg = 0
    if hashrates.exists():
        hashrate_min = min(hashrates)
        hashrate_max = max(hashrates)
        hashrate_avg = sum(hashrates) / len(hashrates)

    hourly_result = HourlyWorkerResult(
        date=start,
        worker_id=worker_id,
        up_time=up_time,
        down_time=down_time,
        hashrate_min=hashrate_min,
        hashrate_max=hashrate_max,
        hashrate_avg=hashrate_avg
    )
    hourly_result.save()

    pooltimes = calc_times(worker_results.values('date', 'pool_id'), 'pool_id')
    for pool_id, time in pooltimes.items():
        hourly_pooltime = HourlyPoolTime(pool_id=pool_id, time=time, result_id=hourly_result.id)
        hourly_pooltime.save()

    cointimes = calc_times(worker_results.values('date', 'coin_id'), 'coin_id')
    for coin_id, time in cointimes.items():
        hourly_cointime = HourlyCoinTime(coin_id=coin_id, time=time, result_id=hourly_result.id)
        hourly_cointime.save()


@shared_task(name='archive_all_daily')
def archive_all_daily():
    """
    Should run after midnight every day.
    Loops over all workers and produces daily reports in separate tasks.
    Looks back and creates missing reports.
    :return:
    """
    _yesterday = timezone.now().date() - timezone.timedelta(days=1)
    for worker in Worker.objects.filter(disabled=False):
        yesterday = _yesterday
        while yesterday >= worker.create_date.date() and not DailyWorkerResult.objects.filter(worker=worker,
                                                                                              date=yesterday).exists():
            archive_worker_daily.delay(worker.id, str(yesterday))
            yesterday -= timezone.timedelta(days=1)


@shared_task(name='archive_worker_daily')
def archive_worker_daily(worker_id, yesterday):
    """
    Creates daily report per worker.
    :param worker_id:
    :param yesterday:
    :return:
    """

    hourly_results = HourlyWorkerResult.objects.filter(worker_id=worker_id, date__date=yesterday)
    if not hourly_results.exists():
        logger.info('No hourly worker results {}'.format(worker_id))
        return
    hourly_results = hourly_results.aggregate(up_time=Sum('up_time'), down_time=Sum('down_time'),
                                              hashrate_min=Min('hashrate_min'), hashrate_max=Max('hashrate_max'),
                                              hashrate_avg=Avg('hashrate_avg'))

    daily_result = DailyWorkerResult(
        date=yesterday,
        worker_id=worker_id,
        up_time=hourly_results['up_time'],
        down_time=hourly_results['down_time'],
        hashrate_min=hourly_results['hashrate_min'],
        hashrate_max=hourly_results['hashrate_max'],
        hashrate_avg=hourly_results['hashrate_avg'],
    )

    daily_result.save()

    hourly_results_ids = hourly_results.values_list('id', flat=True)

    pool_times = HourlyPoolTime.objects.filter(result__in=hourly_results_ids).values('pool_id').annotate(
        time_sum=Sum('time'))

    for p in pool_times:
        daily_pooltime = DailyPoolTime(result_id=daily_result.id, pool_id=p['pool_id'], time=p['time_sum'])
        daily_pooltime.save()

    coin_times = HourlyCoinTime.objects.filter(result__in=hourly_results_ids).values('coin_id').annotate(
        time_sum=Sum('time'))

    for c in coin_times:
        daily_cointime = DailyCoinTime(result_id=daily_result.id, coin_id=c['coin_id'], time=p['time_sum'])
        daily_cointime.save()


@shared_task(name='archive_all_weekly')
def archive_all_weekly():
    """
    Should run at 1:00am every Monday.
    Loops over all workers and produces weekly reports in separate tasks.
    Will loop backwards over weeks trying to fill any missing.
    :return:
    """
    _end_date = timezone.now().date()
    week_day = _end_date.weekday()
    if week_day > 0:
        _end_date -= timezone.timedelta(days=week_day)

    one_week = timezone.timedelta(days=7)
    _start_date = _end_date - timezone.timedelta(days=7)

    for worker in Worker.objects.filter(disabled=False):
        start_date = _start_date
        end_date = _end_date
        while start_date >= worker.create_date.date() and not WeeklyWorkerResult.objects.filter(worker=worker,
                                                                                                date=start_date).exists():
            archive_worker_weekly.delay(worker.id, str(start_date), str(end_date))
            start_date -= one_week
            end_date -= one_week


@shared_task(name='archive_worker_weekly')
def archive_worker_weekly(worker_id, start_date, end_date):
    """
    Called by archive all weekly
    Creates weekly report per worker
    :param worker_id:
    :param start_date:
    :param end_date:
    :return:
    """
    daily_results = DailyWorkerResult.objects.filter(worker_id=worker_id, date__range=(start_date, end_date))
    if not daily_results.exists():
        return
    daily_results = daily_results.aggregate(up_time=Sum('up_time'), down_time=Sum('down_time'),
                                            hashrate_min=Min('hashrate_min'), hashrate_max=Max('hashrate_max'),
                                            hashrate_avg=Avg('hashrate_avg'))

    weekly_result = WeeklyWorkerResult(
        date=start_date,
        worker_id=worker_id,
        up_time=daily_results['up_time'],
        down_time=daily_results['down_time'],
        hashrate_min=daily_results['hashrate_min'],
        hashrate_max=daily_results['hashrate_max'],
        hashrate_avg=daily_results['hashrate_avg'],
    )

    weekly_result.save()

    daily_results_ids = daily_results.values_list('id', flat=True)

    pool_times = DailyPoolTime.objects.filter(result__in=daily_results_ids).values('pool_id').annotate(
        time_sum=Sum('time'))

    for p in pool_times:
        weekly_pooltime = WeeklyPoolTime(result_id=weekly_result.id, pool_id=p['pool_id'], time=p['time_sum'])
        weekly_pooltime.save()

    coin_times = DailyCoinTime.objects.filter(result__in=daily_results_ids).values('coin_id').annotate(
        time_sum=Sum('time'))

    for c in coin_times:
        weekly_cointime = WeeklyCoinTime(result_id=weekly_result.id, coin_id=c['coin_id'], time=p['time_sum'])
        weekly_cointime.save()


def first_of_month(start_date):
    """
    Takes a date and returns same date with day=1
    :param start_date:
    :return:
    """
    return datetime.date(start_date.year, start_date.month, 1)


def previous_month(start_date):
    """
    Takes a date, subtracts 1 month, adjusts year if needed.  Always returns 1st of month.
    :param start_date:
    :return:
    """
    month = start_date.month - 1 if start_date.month > 1 else 12
    year = start_date.year if month < 12 else start_date.year - 1
    return datetime.date(year, month, 1)


@shared_task(name='archive_all_monthly')
def archive_all_monthly():
    """
    Should run at 1:00am on the 1st of each month.
    Loops over all workers and produces monthly reports in separate tasks.
    Will loop back over months and add missing.
    :return:
    """
    one_day = timezone.timedelta(days=1)
    _end_date = first_of_month(timezone.now()) - one_day

    _start_date = previous_month(_end_date)

    for worker in Worker.objects.filter(disabled=False):
        start_date = _start_date
        end_date = _end_date

        while start_date >= worker.create_date.date() and not MonthlyWorkerResult.objects.filter(worker=worker,
                                                                                                 date=start_date).exists():
            archive_worker_monthly.delay(worker.id, str(start_date), str(end_date))

            end_date = start_date - one_day
            start_date = previous_month(start_date)


@shared_task(name='archive_worker_monthly')
def archive_worker_monthly(worker_id, start_date, end_date):
    """
    Creates monthly report per worker
    :param worker_id:
    :param start_date:
    :param end_date:
    :return:
    """
    daily_results = DailyWorkerResult.objects.filter(worker_id=worker_id, date__range=(start_date, end_date))
    if not daily_results.exists():
        return
    daily_results = daily_results.aggregate(up_time=Sum('up_time'), down_time=Sum('down_time'),
                                            hashrate_min=Min('hashrate_min'), hashrate_max=Max('hashrate_max'),
                                            hashrate_avg=Avg('hashrate_avg'))

    monthly_result = MonthlyWorkerResult(
        date=start_date,
        worker_id=worker_id,
        up_time=daily_results['up_time'],
        down_time=daily_results['down_time'],
        hashrate_min=daily_results['hashrate_min'],
        hashrate_max=daily_results['hashrate_max'],
        hashrate_avg=daily_results['hashrate_avg'],
    )

    monthly_result.save()

    daily_results_ids = daily_results.values_list('id', flat=True)

    pool_times = DailyPoolTime.objects.filter(result__in=daily_results_ids).values('pool_id').annotate(
        time_sum=Sum('time'))

    for p in pool_times:
        monthly_pooltime = MonthlyPoolTime(result_id=monthly_result.id, pool_id=p['pool_id'], time=p['time_sum'])
        monthly_pooltime.save()

    coin_times = DailyCoinTime.objects.filter(result__in=daily_results_ids).values('coin_id').annotate(
        time_sum=Sum('time'))

    for c in coin_times:
        monthly_cointime = MonthlyCoinTime(result_id=monthly_result.id, coin_id=c['coin_id'], time=p['time_sum'])
        monthly_cointime.save()


@shared_task(name='delete_all_old_results')
def delete_all_old_results():
    """
    Should be run 3am every day
    Loops all workers and deletes old results as per settings on the worker
    :return:
    """
    for worker in Worker.objects.filter(disabled=False).values('id'):
        delete_old_results.delay(worker['id'], settings.MAX_RESULTS_AGE, settings.MAX_HOURLY_AGE,
                                        settings.MAX_DAILY_AGE, settings.MAX_WEEKLY_AGE, settings.MAX_MONTHLY_AGE)


@shared_task(name='delete_old_results')
def delete_old_results(worker_id, max_results_age, max_hourly_age, max_daily_age, max_weekly_age,
                              max_monthly_age):
    """
    Deletes status results and archived results per worker according to it's settings
    :param worker_id:
    :param max_results_age:
    :param max_hourly_age:
    :param max_daily_age:
    :param max_weekly_age:
    :param max_monthly_age:
    :return:
    """

    now = timezone.now()

    max_results_date = now - timezone.timedelta(days=max_results_age)
    if HourlyWorkerResult.objects.filter(worker_id=worker_id, date__gte=max_results_date).exists():
        # Don't delete worker results unless the hourly results exist for them.
        WorkerResult.objects.filter(worker_id=worker_id, date__lt=max_results_date).delete()

    max_hourly_date = now - timezone.timedelta(days=max_hourly_age)
    if DailyWorkerResult.objects.filter(worker_id=worker_id, date__gte=max_hourly_date.date()).exists():
        # Don't delete hourly results unless the daily results exist for them.
        HourlyWorkerResult.objects.filter(worker_id=worker_id, date__lt=max_hourly_date).delete()

    max_daily_date = (now - timezone.timedelta(days=max_daily_age * 31)).date()
    if WeeklyWorkerResult.objects.filter(worker_id=worker_id, date__gte=max_daily_date).exists() \
        and MonthlyWorkerResult.objects.filter(worker_id=worker_id, date__gte=max_daily_date):
        # Don't delete daily results unless the weekly and monthly results exist for them.
        DailyWorkerResult.objects.filter(worker_id=worker_id, date__lt=max_daily_date).delete()

    max_weekly_date = (now - timezone.timedelta(days=max_weekly_age * 31)).date()
    WeeklyWorkerResult.objects.filter(worker_id=worker_id, date__lt=max_weekly_date).delete()

    max_monthly_date = (now - timezone.timedelta(days=max_monthly_age * 31)).date()
    MonthlyWorkerResult.objects.filter(worker_id=worker_id, date__lt=max_monthly_date)

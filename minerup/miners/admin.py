from django.contrib import admin

from miners.models import Worker, Wallet, WorkerResult, MonthlyWorkerResult, WeeklyWorkerResult, DailyWorkerResult, \
    HourlyWorkerResult, HourlyPoolTime, MonthlyPoolTime, DailyPoolTime, WeeklyPoolTime, HourlyCoinTime, DailyCoinTime, \
    WeeklyCoinTime, MonthlyCoinTime


class WorkerInline(admin.TabularInline):
    readonly_fields = ['status', 'last_tested', 'last_pool', 'last_wallet', 'last_hashrate']
    model = Worker
    extra = 0


class WalletInline(admin.TabularInline):
    model = Wallet
    extra = 0


@admin.register(Worker)
class WorkerAdmin(admin.ModelAdmin):
    readonly_fields = ['status', 'last_tested', 'up_since', 'failure_started', 'last_pool', 'last_wallet',
                       'last_hashrate']


admin.site.register(Wallet)


@admin.register(WorkerResult)
class WorkerResultAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    readonly_fields = list_display = ['worker', 'status', 'hashrate', 'pool', 'coin', 'date']
    list_filter = ['date', 'worker', 'status', 'pool', 'coin']
    search_fields = ['worker__name']

    def has_add_permission(self, request):
        return False


class PoolTimeInline(admin.TabularInline):
    extra = 0
    readonly_fields = ['pool', 'time']

    def has_add_permission(self, request):
        return False


class HourlyPoolTimeInline(PoolTimeInline):
    model = HourlyPoolTime


class DailyPoolTimeInline(PoolTimeInline):
    model = DailyPoolTime


class WeeklyPoolTimeInline(PoolTimeInline):
    model = WeeklyPoolTime


class MonthlyPoolTimeInline(PoolTimeInline):
    model = MonthlyPoolTime


class CoinTimeInline(admin.TabularInline):
    extra = 0
    readonly_fields = ['coin', 'time']

    def has_add_permission(self, request):
        return False


class HourlyCoinTimeInline(CoinTimeInline):
    model = HourlyCoinTime


class DailyCoinTimeInline(CoinTimeInline):
    model = DailyCoinTime


class WeeklyCoinTimeInline(CoinTimeInline):
    model = WeeklyCoinTime


class MonthlyCoinTimeInline(CoinTimeInline):
    model = MonthlyCoinTime


# class WorkerBalanceInline(admin.TabularInline):
#     extra = 0
#     readonly_fields = ['coin', 'balance']
#
#     def has_add_permission(self, request):
#         return False
#
#
# class HourlyWorkerBalanceInline(WorkerBalanceInline):
#     model = HourlyWorkerBalance
#
#
# class DailyWorkerBalanceInline(WorkerBalanceInline):
#     model = DailyWorkerBalance
#
#
# class WeeklyWorkerBalanceInline(WorkerBalanceInline):
#     model = WeeklyWorkerBalance
#
#
# class MonthlyWorkerBalanceInline(WorkerBalanceInline):
#     model = MonthlyWorkerBalance


class ArchivedWorkerResultAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    readonly_fields = list_display = ['worker', 'date', 'up_time', 'down_time', 'hashrate_min', 'hashrate_max',
                                      'hashrate_avg']
    list_filter = ['date', 'worker']
    search_fields = ['worker__name']

    def has_add_permission(self, request):
        return False


@admin.register(HourlyWorkerResult)
class HourlyWorkerResultAdmin(ArchivedWorkerResultAdmin):
    inlines = [HourlyPoolTimeInline, HourlyCoinTimeInline]


@admin.register(DailyWorkerResult)
class DailyWorkerResultAdmin(ArchivedWorkerResultAdmin):
    inlines = [DailyPoolTimeInline, DailyCoinTimeInline]


@admin.register(WeeklyWorkerResult)
class WeeklyWorkerResultAdmin(ArchivedWorkerResultAdmin):
    inlines = [WeeklyPoolTimeInline, WeeklyCoinTimeInline]


@admin.register(MonthlyWorkerResult)
class MonthlyWorkerResultAdmin(ArchivedWorkerResultAdmin):
    inlines = [MonthlyPoolTimeInline, MonthlyCoinTimeInline]

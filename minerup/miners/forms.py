from django import forms
from django.forms import inlineformset_factory

from miners.models import Worker, Wallet
from pools.models import Pool
from usercustom.models import User


class WorkerForm(forms.ModelForm):
    user = forms.IntegerField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Worker
        fields = ['name', 'user']


class PoolsForm(forms.Form):
    pools = forms.ModelMultipleChoiceField(Pool.objects.all())

WalletFormSet = inlineformset_factory(User, Wallet, extra=0, can_delete=True, fields=['coin', 'address', 'email'])

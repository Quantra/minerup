import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.template.defaultfilters import timesince, floatformat
from django.urls import reverse_lazy, reverse
from django.utils import dateformat, timezone
from django.utils.dateparse import parse_datetime
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, FormView

from miners.forms import WorkerForm, PoolsForm, WalletFormSet
from miners.models import Worker, Wallet
from miners.tasks import test_worker
from pools.models import Pool


@login_required
def worker_list(request):
    """
    Dashboard view.
    :param request:
    :return:
    """
    workers = request.user.workers.all().select_related('last_pool', 'last_wallet')
    wallets = Wallet.objects.filter(user=request.user).select_related('coin')

    return render(request, 'miners/worker_list.html', {
        'workers': workers,
        'wallets': wallets
    })


@login_required
def worker_list_ajax(request):
    """
    Ajax refresh dashboard
    :param request:
    :return:
    """
    if not request.is_ajax():
        return HttpResponseRedirect('/')
    if request.method != 'POST':
        return JsonResponse({}, status=500)

    worker_data = json.loads(request.POST.get('worker_data'))
    data = {'workers_data': {}, }
    for w in Worker.objects.filter(id__in=worker_data.keys()):
        if parse_datetime(worker_data[str(w.id)]['last_test_date']) < w.last_tested:
            # Only send back data for websites which have had a new test made since last ajax request.

            # todo send pool/wallet info and update if changed
            last_pool = w.last_pool.url if w.last_pool else None
            last_wallet = w.last_wallet.address if w.last_wallet else None
            data['workers_data'][w.id] = {
                'status_changed': True if worker_data[str(w.id)]['status_num'] != str(w.status) else False,
                'last_test': dateformat.format(w.last_tested, 'c'),
                'timing': timesince(w.up_since if w.status == Worker.ONLINE else w.failure_started, timezone.now()),
                'timing_label': u'Uptime' if w.status == Worker.ONLINE else u'Downtime',
                'status_num': w.status,
                'status': w.get_status_display(),
                'status_class': w.get_status_display().lower(),
                'pool': last_pool,
                'wallet': last_wallet,
                'hashrate': floatformat(w.last_hashrate, 2) + ' Mh/s',
                'pool_changed': True if worker_data[str(w.id)].get('pool') != last_pool else False,
                'wallet_changed': True if worker_data[str(w.id)].get('wallet') != last_wallet else False
            }

    return JsonResponse(data)


@method_decorator(login_required, name='dispatch')
class AddWorker(CreateView):
    """
    Add new worker form.
    """
    model = Worker
    form_class = WorkerForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(AddWorker, self).form_valid(form)


@login_required
def delete_worker(request):
    """
    Delete worker.
    :param request:
    :return:
    """
    if request.method != 'POST':
        raise Http404

    worker = get_object_or_404(Worker, id=request.POST.get('worker_id'))

    if worker.user != request.user:
        raise Http404

    worker.delete()

    return HttpResponseRedirect(reverse('home'))


@login_required
def toggle_worker(request):
    """
    Disable/enable worker.
    :param request:
    :return:
    """
    if request.method != 'POST' or not request.is_ajax():
        raise Http404

    worker = get_object_or_404(Worker, id=request.POST.get('worker_id'))

    if worker.user != request.user:
        raise Http404

    worker.disabled = not worker.disabled
    worker.save()

    if not worker.disabled:
        test_worker.delay(worker.id)

    return JsonResponse({
        'worker_id': worker.id,
        'status': 'Disabled' if worker.disabled else worker.get_status_display()
    })


@method_decorator(login_required, name='dispatch')
class EditPools(FormView):
    form_class = PoolsForm
    template_name = 'miners/user_pools_form.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        self.request.user.pools.clear()
        for p in form.cleaned_data['pools']:
            self.request.user.pools.add(p)
        return super(EditPools, self).form_valid(form)

    def get_initial(self):
        print(self.request.user.pools.all())
        return {'pools': Pool.objects.filter(users=self.request.user)}


@login_required
def edit_wallets(request):
    if request.method == 'POST':
        formset = WalletFormSet(request.POST, instance=request.user)
        print('posted', formset.is_valid())
        if formset.is_valid():
            print('save something here')
            formset.save()
            return HttpResponseRedirect(reverse('home'))
    else:
        formset = WalletFormSet(instance=request.user, initial=[{'email': '123'}])

    return render(request, 'miners/wallet_formset.html', {'formset': formset})


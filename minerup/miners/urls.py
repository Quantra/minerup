from django.conf.urls import url

from miners.views import worker_list, worker_list_ajax, AddWorker, delete_worker, toggle_worker, EditPools, edit_wallets

urlpatterns = [
    url(r'^add-worker/$', AddWorker.as_view(), name='miners_add_worker'),
    url(r'^delete-worker/$', delete_worker, name='miners_delete_worker'),
    url(r'^toggle-worker/$', toggle_worker, name='miners_toggle_worker'),
    url(r'^edit-pools/$', EditPools.as_view(), name='miners_edit_pools'),
    url(r'^edit-wallets/$', edit_wallets, name='miners_edit_wallets'),
    url(r'^refresh-worker-list/$', worker_list_ajax, name='miners_refresh_list'),
    url(r'^$', worker_list, name='home'),
]

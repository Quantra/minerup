$(document).ready(function() {
	$('input,textarea').placeholder({force: false});
	$('form select').select2({
		minimumResultsForSearch: Infinity,
		width: '100%'
	});
});
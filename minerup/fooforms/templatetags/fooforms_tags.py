from django import template
register = template.Library()


@register.filter(name='field_template')
def field_template(field):
    return 'foofields/' + field.field.widget.__class__.__name__ + '.html'

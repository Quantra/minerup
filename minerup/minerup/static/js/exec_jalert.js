$(document).ready(function() {
    $('.confirm-remove-modal').click(function(e) {
        e.preventDefault();
        var form = $(this).siblings('form');
        $.jAlert({
            'title': 'Confirm',
            'content': 'Are you sure you want to delete?',
            'closeBtn':false,
            'replaceOtherAlerts': true,
            'btns':[
                {
                    'text':'Yes',
                    'theme':'green',
                    'closeAlert':true,
                    'onClick':function(e, btn) {
                        if (btn.attr('data-disabled')) {return;}
                        btn.attr('data-disabled', true);
                        e.preventDefault();
                        form.submit();
                    }
                },
                {'text':'No', 'theme':'red'}
            ]
        });
    })
});
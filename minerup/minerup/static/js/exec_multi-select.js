$(document).ready(function() {
	$('#id_pools').multiSelect({
		selectableHeader: "<h3>Available Pools</h3><div class='search-wrapper'><input type='text' class='search-input' autocomplete='off' placeholder='Search'></div>",
		selectionHeader: "<h3>Selected Pools</h3><div class='search-wrapper'><input type='text' class='search-input' autocomplete='off' placeholder='Search'></div>",
		afterInit: function(ms){
			var that = this,
				$selectableSearch = that.$selectableUl.prev().children('input'),
				$selectionSearch = that.$selectionUl.prev().children('input'),
				selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
				selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
			
			that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
				.on('keydown', function(e){
					if (e.which === 40){
						that.$selectableUl.focus();
						return false;
					}
				});

			that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
				.on('keydown', function(e){
					if (e.which == 40){
						that.$selectionUl.focus();
						return false;
					}
				});
		},
		afterSelect: function(){
			this.qs1.cache();
			this.qs2.cache();
		},
		afterDeselect: function(){
			this.qs1.cache();
			this.qs2.cache();
		}
	});
});
$(document).ready(function () {
    $('.post-link').click(function(e) {
        if ($(this).attr('data-disabled')) {return;}
        $(this).attr('data-disabled', true);
        e.preventDefault();
        $(this).siblings('form').submit();
    });
});
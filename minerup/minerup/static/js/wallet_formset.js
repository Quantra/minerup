// var empty_form;
$(document).ready(function() {
    $('#id_wallets-__prefix__-email').attr('value', def_email);
    var $empty_form = $('.emptyform');

    var empty_form = $empty_form.html();
    var $total_forms = $('#id_wallets-TOTAL_FORMS');
    var total_forms;
    var $formset_wrapper = $('.formset-wrapper');

    $('.add-another').click(function(e) {
        e.preventDefault();

        total_forms = $total_forms.val();

        $formset_wrapper.append(empty_form.replace(/__prefix__/g, total_forms));

        $('input,textarea').placeholder({force: false});
        $('select').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        total_forms ++;
        $total_forms.val(total_forms);
    });
});
//  Get CSRF token from cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

//  Setup AJAX to send CSRF token as standard on POST
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

var worker_ids;

$(document).ready(function () {
    $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        percentPosition: true
    });

    worker_ids = $('.worker-wrapper').map(function() {return this.id;}).get();

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        error: renderError,
        dataType: "json"
    });

    setInterval(function () {refreshWorkers();}, 30 * 1000);

    $('.fadetime').each(function() {
        $(this).on('change.livestamp', function(event, from, to) {
            event.preventDefault(); // Stop the text from changing automatically
            fadeTo(this, to);
        }).livestamp($(this).attr('data-timestamp'));
    });

    $('.toggle-worker').click(function(e) {
        e.preventDefault();
        console.log('toggle worker');
        toggleWorkerAjax(parseInt($(this).parent('.worker').attr('id')));
    });
});

function getWorkersData() {
    var worker_data = {};
    $('.worker-wrapper').each(function () {
        worker_data[this.id] = {
            last_test_date: $(this).attr('data-last_test_date'),
            status_num: $(this).attr('data-status_num'),
            pool: $(this).attr('data-pool'),
            wallet: $(this).attr('data-wallet')
        };
    });
    console.log(worker_data);
    return worker_data;
}


function refreshWorkers() {
    $.ajax({
        url: "refresh-worker-list/",
        type: "POST",
        data: {
            worker_data: JSON.stringify(getWorkersData())
            // worker_ids: worker_ids
        },
        success: renderWorkers
    });
}

function renderWorkers(data) {
    console.log(data);
    for (var worker_id in data['workers_data']) {
        var worker_data = data['workers_data'][worker_id];
        var $worker_wrapper = $('.worker-id'+worker_id);

        // Update last tested date and status num
        $worker_wrapper.attr('data-last_test_date', worker_data['last_test']);
        $worker_wrapper.attr('data-status_num', worker_data['status_num']);
        $worker_wrapper.attr('data-pool', worker_data['pool']);
        $worker_wrapper.attr('data-wallet', worker_data['wallet']);

        // Change labels and timing data if status changed
        if (worker_data['status_changed']) {
            fadeTo($worker_wrapper.find('.status .label'), worker_data['status']);
            $worker_wrapper.find('.worker').removeClass('online offline failing').addClass(worker_data['status_class']);
            $worker_wrapper.find('.timing .label').html(worker_data['timing_label']);
        }
        var $timingvalue = $worker_wrapper.find('.timing .value');
        if ($timingvalue.html() != worker_data['timing']) {fadeTo($timingvalue, worker_data['timing']);}

        if (worker_data['pool_changed']) {
            fadeTo($worker_wrapper.find('.pool'), worker_data['pool']);
        }

        if (worker_data['wallet_changed']) {
            fadeTo($worker_wrapper.find('.wallet'), worker_data['wallet']);
        }

        // Update latency and last test livestamp
        $worker_wrapper.find('.last-test .value').attr('data-livestamp', worker_data['last_test']);

        // Update hashrate
        fadeTo($worker_wrapper.find('.hashrate .value'), worker_data['hashrate']);

    }
}

var disabling = [];
function toggleWorkerAjax(worker_id) {
    console.log(disabling);
    console.log($.inArray(worker_id, disabling));
    if ($.inArray(worker_id, disabling) > 0) {return;}
    disabling.push(worker_id);
    $.ajax({
        url: 'toggle-worker/',
        type: 'POST',
        data: {worker_id: worker_id},
        success: toggleWorker
    })
}


function toggleWorker(data) {
    var worker_id = data['worker_id'];
    var $worker_wrapper =  $('.worker-id'+worker_id);
    $worker_wrapper.toggleClass('disabled');
    fadeTo($worker_wrapper.find('.status .label'), data['status']);
    disabling = disabling.filter(val => val !== worker_id);
}


function renderError(data) {
    // Callback for when an error is returned
    console.log(data);
}


function fadeTo(ele, to) {
    $(ele).stop(true, false).fadeOut(function() {
        $(ele).html(to).fadeIn();
    });
}

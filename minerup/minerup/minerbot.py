from importlib import import_module
from inspect import getmembers, isfunction

from slackbot.bot import respond_to

from slackbot_settings import PLUGINS


@respond_to('^help$')
@respond_to('^help (\S+)')
def bot_help(message, cmd=None):
    """Try `help` for a list of available commands."""

    msg = ''
    if cmd is not None:
        f = available_commands.get(cmd)
        if f:
            message.reply(f.__doc__)
            return
        msg += 'No command found for `{}` \n'.format(cmd)

    msg += 'Available commands: ' + ' '.join(sorted(available_commands.keys()))
    msg += '\n Try `help command` for details about that command.'
    message.reply(msg)
bot_help.botstring = 'help'


available_commands = {}
for p in PLUGINS:
    predicate = lambda f: isfunction(f) and f.__module__ in PLUGINS and not getattr(f, 'notacmd', False)
    predicate.notacmd = True
    available_commands.update({getattr(f[1], 'botstring', f[0]): f[1] for f in getmembers(import_module(p), predicate)})

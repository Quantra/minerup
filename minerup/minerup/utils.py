import requests
from django.core.cache import cache


def cached_json_request(url, params={}, ttl=60):
    """
    Wrapper for python requests.get which first checks cache for json.
    Returns parsed json if successful.  None on failure.  Empty dict if bad request.
    :param ttl:
    :param url:
    :param params:
    :return:
    """

    cache_key = 'CJR' + url + ''.join('{}{}'.format(k, v) for k, v in params.items())
    json = cache.get(url)
    print(url, params)

    if not json:
        try:
            r = requests.get(url, params, timeout=20)
            r.raise_for_status()
            # print(r)
            json = r.json()
            cache.set(cache_key, json, ttl)
            # todo return the actual error here so can be handled further down ie might not want to mark a worker as
            # todo offline when there is a connection error... Webserver network could be down...
            #
            # todo this is hard to handle.  need to check if ALL tests throw same error then ignore and test later?
            # todo or only consider these caveats when looking at last pool/wallet but if user changes could be weird...
            # todo maybe only concern ourselves with ConnectionError?
        except requests.HTTPError:
            print('HTTP error. Bad request or problem with api server/network.')
            json = {}
        except ValueError:
            print('JSON not returned. Bad request or problem with api server/network.')
            json = {}
        except requests.ConnectionError:
            print('Connection error. Problem with our network or api network or the internet.')
            json = {}

    # print(json)
    return json

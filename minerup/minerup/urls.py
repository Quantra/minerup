"""minerup URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import password_reset, logout_then_login, login
from django.conf import settings
from django.views.generic import TemplateView

from usercustom.forms import CustomPasswordResetForm
from usercustom.views import UpdateUserView

urlpatterns = [
    # Admin
    url(r'^grappelli/', include('grappelli.urls')),    # grappelli URLS
    url(r'^admin/password_reset/$', password_reset, name='admin_password_reset'),
    url(r'^admin/password_reset/done/$', auth_views.password_reset_done, name='admin_password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', auth_views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='admin_password_reset_complete'),
    url(r'^admin/', admin.site.urls),

    #Accounts
    url(r'^accounts/password_reset/', password_reset, {'password_reset_form': CustomPasswordResetForm}, name='password_reset'),
    url(r'^accounts/logout/', logout_then_login, name='logout_then_login'),
    url(r'^accounts/login/', login, name='login'),
    url(r'^accounts/edit/', login_required(UpdateUserView.as_view()), name='usercustom_update_user'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/', login),

    url(r'^faq/', TemplateView.as_view(template_name='faq.html'), name='faq'),

    #Apps
    url(r'', include('miners.urls')),
]

if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

from django.db import models


class VisibleManager(models.Manager):
    def get_queryset(self):
        return super(VisibleManager, self).get_queryset().filter(visible=True)


class VisibleModel(models.Model):
    visible = models.BooleanField(default=True, help_text=u'Uncheck to hide this.')

    class Meta:
        abstract = True


class SEOModel(models.Model):
    meta_title = models.CharField(max_length=180, blank=True, null=True, verbose_name='SEO Title',
                                  help_text=u'The title of the page as it should appear on the website.  Overrides \
                                  normal page title.')
    meta_keywords = models.TextField(blank=True, null=True, verbose_name='SEO Keywords',
                                     help_text=u'A comma seperated list of keywords used by search engines to help \
                                     index your site.  Not visible on site, used for SEO. Overrides default keywords.')
    meta_description = models.TextField(blank=True, null=True, verbose_name='SEO Description',
                                        help_text=u'Description used by search engines to help index your site.  Not \
                                        visible on site, used for SEO. Overrides default description.')

    class Meta:
        abstract = True


class AutoDateModel(models.Model):
    create_date = models.DateTimeField('Creation Date', auto_now_add=True)
    mod_date = models.DateTimeField('Last Modified', auto_now=True)

    class Meta:
        abstract = True


class NoDeleteModel(models.Model):
    def delete(self, *args, **kwargs):
        # Settings cannot be deleted!
        return None

    class Meta:
        abstract = True


class PositionModel(models.Model):
    position = models.PositiveIntegerField(default=0, help_text=u'Set a custom ordering. Lower numbers appear first.')

    class Meta:
        abstract = True
        ordering = ['position', ]

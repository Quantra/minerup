import os

from minerup.settings_pro_secret import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SETTINGS_DIR = os.path.join(BASE_DIR, 'minerup')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
DEBUG_TOOLBAR = DEBUG


def show_toolbar(request):
    return DEBUG_TOOLBAR


DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': show_toolbar}
SHOW_TEMPLATE_CONTEXT = True

ALLOWED_HOSTS = [
    '159.203.7.187',
    '.minertopia.org'
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
MEDIA_ROOT = os.path.join('/', 'home', 'quantra', 'minerup_static', 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join('/', 'home', 'quantra', 'minerup_static', 'static')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(SETTINGS_DIR, 'static'),
)

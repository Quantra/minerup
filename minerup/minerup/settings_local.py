import os

from minerup.settings_local_secret import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SETTINGS_DIR = os.path.join(BASE_DIR, 'minerup')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEBUG_TOOLBAR = DEBUG


def show_toolbar(request):
    return DEBUG_TOOLBAR


DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': show_toolbar}
SHOW_TEMPLATE_CONTEXT = True

ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1'
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
MEDIA_ROOT = os.path.join(SETTINGS_DIR, 'static', 'media')

MEDIA_URL = '/static/media/'

STATIC_ROOT = os.path.join(SETTINGS_DIR, 'static_root')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(SETTINGS_DIR, 'static'),
)

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": 'localhost:6379',
        # "OPTIONS": {
        #     "PASSWORD": redis_url.password,
        #     "DB": 0,
        # }
    },
    "memcache": {
        "BACKEND": "django.core.cache.backends.memcached.MemcachedCache",
        "LOCATION": 'localhost:11211',
    }
}

CELERY_BROKER_URL = 'redis://localhost:6379'
# CELERY_RESULT_BACKEND = 'redis://localhost:6379'

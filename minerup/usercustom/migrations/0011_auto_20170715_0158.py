# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-15 01:58
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('usercustom', '0010_auto_20170714_0308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='slack_token',
            field=models.UUIDField(default=uuid.UUID('da2edec8-d9dc-4aa5-836a-345ef2b845b6'), unique=True),
        ),
    ]

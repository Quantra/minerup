# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-15 01:59
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('usercustom', '0011_auto_20170715_0158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='slack_token',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
    ]

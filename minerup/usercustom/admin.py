from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib import messages
from django.db import IntegrityError

from miners.admin import WorkerInline, WalletInline
from usercustom.models import User


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput, required=False)

    class Meta:
        model = User
        fields = ('email', 'is_admin')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(help_text="Raw passwords are not stored, so there is no way to see "
                                                   "this user's password, but you can change the password "
                                                   "using <a href=\"../password/\">this form</a>."
                                         )

    class Meta:
        model = User
        fields = ('email', 'password', 'is_active', 'is_admin')

    def clean_password(self):
        return self.initial["password"]


def send_pw_email(modeladmin, request, queryset):
    for obj in queryset:
        obj.send_password_set_email()
    messages.success(request, 'Password email sent to {} users'.format(queryset.count()))
send_pw_email.short_description = "Send password set email to selected users."


@admin.register(User)
class UserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = (
        'email', 'first_name', 'last_name', 'is_active', 'is_admin', 'is_superuser', 'mod_date', 'date_joined')
    list_filter = ('date_joined', 'mod_date', 'is_active', 'is_admin', 'is_superuser')
    readonly_fields = ['date_joined', 'mod_date', 'slack_token']
    fieldsets = (
        ('Info', {'classes': ('grp-collapse grp-open',), 'fields': ('date_joined', 'mod_date',)}),
        ('User', {'classes': ('grp-collapse grp-open',), 'fields': ('email', 'password')}),
        ('Personal info',
         {'classes': ('grp-collapse grp-open',), 'fields': ('first_name', 'last_name', 'slack_id', 'slack_token')}),
        ('Admin Site Permissions', {'classes': ('grp-collapse grp-open',), 'fields': (
            'is_active', 'is_admin', 'is_superuser', 'groups', 'user_permissions')}),
        ('Miners', {'classes': ('grp-collapse grp-open',), 'fields': ('pools',)}),
        ('Alerts', {'classes': ('grp-collapse grp-open',), 'fields': ('slack_alerts', 'email_alerts',)})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'is_admin', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'last_name', 'first_name', 'tel')
    actions = (send_pw_email,)

    ordering = ('email',)
    filter_horizontal = ['groups', 'user_permissions', 'pools']
    inlines = [WalletInline, WorkerInline]

    def get_formsets_with_inlines(self, request, obj=None):
        # Hide inlines when creating objects.
        if not obj:
            return []
        return super(UserAdmin, self).get_formsets_with_inlines(request, obj)

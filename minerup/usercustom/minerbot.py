import re

from slackbot.bot import respond_to

from usercustom.models import User


@respond_to('^linkaccount (\S+)$', re.IGNORECASE)
def link_account(message, slack_token):
    """
    Link your slack user with your user account by using your slack token to receive notifications via slack.
    """
    try:
        user = User.objects.get(slack_token=slack_token)
    except User.DoesNotExist:
        message.reply('No user found for slack token: {}'.format(slack_token))
        return

    if user.slack_id:
        message.reply('This account already has a slack user linked.  `unlinkaccount` first.')
        return

    user.slack_id = message._get_user_id()
    user.save()

    message.reply('Linked with account: {}'.format(user))
link_account.botstring = 'linkaccount'


@respond_to('^unlinkaccount$')
def unlink_account(message):
    """
    Unlink your slack account.
    """
    try:
        user = User.objects.get(slack_id=message._get_user_id())
    except User.DoesNotExist:
        message.reply('Account not linked yet.')
        return

    user.slack_id = None
    user.save()

    message.reply('Unlinked account: {}'.format(user))
unlink_account.botstring = 'unlinkaccount'

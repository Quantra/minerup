from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.edit import UpdateView

from usercustom.forms import ConfirmForm, UpdateUserForm

User = get_user_model()


@login_required
def deactivate(request):
    if request.method == 'POST':
        form = ConfirmForm(request.POST)
        if form.is_valid():
            request.user.is_active = False
            request.user.save()

            return HttpResponseRedirect(reverse('home'))
    else:
        form = ConfirmForm()

    return render(request, 'deactivate_account.html', {'form': form})


class UpdateUserView(UpdateView):
    model = User
    form_class = UpdateUserForm
    template_name = 'update_user_form.html'
    success_url = reverse_lazy('home')

    def get_object(self, **kwargs):
        return self.request.user

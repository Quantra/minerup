from django import forms
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.tokens import default_token_generator

User = get_user_model()


class UpdateUserForm(forms.ModelForm):
    # first_name = forms.CharField(max_length=150)
    # last_name = forms.CharField(max_length=150)

    class Meta:
        model = User
        fields = [
            'email',
            'email_alerts',
            'slack_alerts'
        ]


class ConfirmForm(forms.Form):
    confirmed = forms.BooleanField()


class CustomPasswordResetForm(PasswordResetForm):
    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):

        email = self.cleaned_data['email']
        if list(self.get_users(email)):
            try:
                super(CustomPasswordResetForm, self).save()
                messages.success(request, 'Password reset email sent to %s' % email)
            except Exception as e:
                messages.error(request, 'Failed to send email to %s with exception %s' % (email, e))
        else:
            messages.error(request, 'No user with email %s found.' % email)

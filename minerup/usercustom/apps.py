from django.apps import AppConfig


class UsercustomConfig(AppConfig):
    name = 'usercustom'

    def ready(self):
        import usercustom.signals

import uuid

from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.mail.message import EmailMessage
from django.db import models
from django.template.loader import render_to_string

from pools.models import Pool


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_admin=False):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            is_admin=is_admin
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, password, email):
        user = self.create_user(email, password=password, is_admin=True)
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    date_joined = models.DateTimeField(auto_now_add=True)
    mod_date = models.DateTimeField(auto_now=True)

    email = models.EmailField(verbose_name='email address', max_length=150, unique=True)
    email_alerts = models.BooleanField(default=True)
    first_name = models.CharField(max_length=150, blank=True, null=True)
    last_name = models.CharField(max_length=150, blank=True, null=True)

    is_active = models.BooleanField(default=True, help_text=u'Only active users can login.')
    is_admin = models.BooleanField(default=False,
                                   help_text=u'Check this box to give the user access to the admin site.')

    slack_id = models.CharField(max_length=32, blank=True, null=True)
    slack_token = models.UUIDField(max_length=32, default=uuid.uuid4, unique=True)
    slack_alerts = models.BooleanField(default=True)

    pools = models.ManyToManyField(Pool, related_name='users', blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        msg = EmailMessage(subject, message, from_email, [self.email], **kwargs)
        msg.send()

    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        return self.is_admin

    def send_password_set_email(self):
        email_body = render_to_string('emails/user_password_email.txt', {
            'domain': Site.objects.get_current().domain,
            'email': self.email,
            'name': self.get_short_name()
        })
        send_mail('Minerup - Set up your password', email_body, None, [self.email, ])

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from usercustom.views import UpdateUserView
from . import views

urlpatterns = [
    url(r'^deactivate/$', views.deactivate, name='usercustom_deactivate'),

    url(r'^update-user/$', login_required(UpdateUserView.as_view()), name='usercustom_update_user'),

    url(r'^update-user/success/$', login_required(TemplateView.as_view(template_name="update_user_success.html")),
        name='usercustom_update_user_success'),

    url(r'^dashboard/$', login_required(TemplateView.as_view(template_name="dashboard.html")), name='dashboard'),
]

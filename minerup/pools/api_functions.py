def test_worker_mt(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    wallet = args[1]
    return not endpoint.get_json(url, wallet).get('workers', {}).get(worker_name, {}).get('offline', True)


def test_worker_dwarf(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    wallet = args[1]
    email = kwargs.get('email', 'mail@example.com')
    return endpoint.get_json(url, wallet=wallet, email=email).get('workers', {}).get(worker_name, {}).get('alive',
                                                                                                          False)


def test_worker_nano(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    wallet = args[1]
    data = endpoint.get_json(url, wallet).get('data', {})
    for d in data:
        if d.get('id') == worker_name:
            return d.get('hashrate', 0) > 0
    return False


def test_worker_hashbag(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    address = args[1]
    if not address:
        return False
    data = endpoint.get_json(url, address=address).get('miners', {})
    for d in data:
        if d.get('ID') == worker_name:
            return True
    return False


def hashrate_worker_none(endpoint, url, *args, **kwargs):
    return 0


def hashrate_worker_mt(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    wallet = args[1]
    hashrate_mod = kwargs.get('hashrate_mod', 1)
    return endpoint.get_json(url, wallet).get('workers', {}).get(worker_name, {}).get('hr', 0) * hashrate_mod


def hashrate_worker_dwarf(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    wallet = args[1]
    email = kwargs.get('email', 'mail@example.com')
    hashrate_mod = kwargs.get('hashrate_mod', 1)
    return endpoint.get_json(url,
                             wallet=wallet,
                             email=email).get('workers', {}).get(worker_name, {}).get('hashrate', 0) * hashrate_mod


def hashrate_worker_nano(endpoint, url, *args, **kwargs):
    worker_name = args[0]
    wallet = args[1]
    hashrate_mod = kwargs.get('hashrate_mod', 1)
    data = endpoint.get_json(url, wallet).get('data', {})
    for d in data:
        if d.get('id') == worker_name:
            return d.get('hashrate', 0) * hashrate_mod
    return 0

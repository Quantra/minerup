from importlib import import_module
from inspect import isfunction, getmembers
from urllib.parse import urljoin

from django.db import models

from minerup.models_base import AutoDateModel
from minerup.utils import cached_json_request
# todo uniqueness on all botstrings
# todo validators
from pools import api_functions


class WrongArgs(Exception): pass


class WrongKwargs(Exception): pass


class ApiEndpoint(AutoDateModel):
    label = models.CharField(max_length=128)
    path = models.CharField(max_length=512)
    request_args = models.PositiveIntegerField(default=0)
    request_kwargs = models.CharField(max_length=512, blank=True, null=True)

    OTHER = 10
    WORKER_INFO = 20
    ENDPOINT_TYPE_CHOICES = (
        (OTHER, 'other'),
        (WORKER_INFO, 'worker info')
    )
    endpoint_type = models.PositiveSmallIntegerField(choices=ENDPOINT_TYPE_CHOICES, default=OTHER)
    # todo make this unique together with pool with a through model/admin form?

    def __str__(self):
        return self.label

    def get_json(self, url, *args, **kwargs):
        # Endpoints take kwargs only but if args are needed set it.  In theory can use both but currently not needed.
        # store list of possible kwargs and map them to args if no kwargs actually provided
        # raise custom exception when args/kwargs don't match
        # todo add some useful info to these exceptions
        args = list(args)
        request_kwargs = self.request_kwargs.split() if self.request_kwargs else []

        if not kwargs:
            if len(args) - len(request_kwargs) != self.request_args:
                raise WrongArgs

            if len(args) - self.request_args != len(request_kwargs):
                raise WrongKwargs

            for k in request_kwargs:
                kwargs[k] = args.pop()
        else:
            if len(args) != self.request_args:
                raise WrongArgs

            for k in request_kwargs:
                if k not in kwargs.keys():
                    raise WrongKwargs

        return cached_json_request(urljoin(url, self.path.format(*args)), kwargs)


class ApiFunction(AutoDateModel):
    endpoint = models.ForeignKey(ApiEndpoint, related_name='apifunctions')

    TEST_WORKER = 'test_worker'
    HASHRATE_WORKER = 'hashrate_worker'
    api_function_choices = (
        (TEST_WORKER, 'test_worker'),
        (HASHRATE_WORKER, 'hashrate_worker')
    )
    api_function = models.CharField(max_length=32, choices=api_function_choices)

    FUNCTION_CHOICES = ((f[0], f[0]) for f in getmembers(import_module('pools.api_functions'), lambda f: isfunction(f)))
    function = models.CharField(max_length=32, choices=FUNCTION_CHOICES)

    def exec_function(self, url, *args, **kwargs):
        # import function, pass through args and kwargs, return result
        result = getattr(api_functions, self.function)(self.endpoint, url, *args, **kwargs)
        # print(result)
        return result

    def __str__(self):
        return self.function


class BotMessage(AutoDateModel):
    endpoint = models.ForeignKey(ApiEndpoint, related_name='messages')
    label = models.CharField(max_length=128)
    message = models.CharField(max_length=512, blank=True, null=True)
    botstring = models.CharField(max_length=32)

    def __str__(self):
        return self.label

    def get_message(self, url, *args, **kwargs):
        if not self.message:
            return
        try:
            json = self.endpoint.get_json(url, *args, **kwargs)
            # print(json)
        except (WrongArgs, WrongKwargs):
            return 'Wrong number of arguments supplied.  Try again dickhead.'

        if not json:
            return 'Bad request or no data received. Check any args you sent.  ' \
                   'Try again later or report this error if it happens every bloody time.'

        try:
            return self.message.format(**json)
        except TypeError:
            return 'Nothing to see here.'


class Coin(AutoDateModel):
    label = models.CharField(max_length=128)
    symbol = models.CharField(max_length=8)

    def __str__(self):
        return self.label


class Pool(AutoDateModel):
    label = models.CharField(max_length=128)
    url = models.URLField()
    endpoints = models.ManyToManyField(ApiEndpoint)
    coins = models.ManyToManyField(Coin, related_name='pools')
    botstring = models.CharField(max_length=32, unique=True)
    hashrate_mod = models.FloatField(default=1)

    def __str__(self):
        return self.label

import re

from slackbot.bot import respond_to

from pools.models import Pool, BotMessage


@respond_to('^test', re.IGNORECASE)
def test(message):
    p = Pool.objects.get(pk=1)
    m = BotMessage.objects.get(pk=1)
    message.reply(m.get_message(p.url, address='0xadd2b0ab56b4c77e74cd7f0ae0716f4c8f454cd5'))


test.notacmd = True


@respond_to('^pools', re.IGNORECASE)
def pools(message):
    """Shows a list of pools.  Try `info poolname` to see available info from that pool."""
    message.reply('Available pools: ' + ' '.join(Pool.objects.values_list('botstring', flat=True)))


@respond_to('^info (\S+)$', re.IGNORECASE)
@respond_to('^info (\S+) (\S+) ?(.*)?$', re.IGNORECASE)
def get_message(message, pool, msg=None, args=''):
    """Get info from one of the pools.  Try `pools` for a list of pools."""
    print(msg, pool, args)
    try:
        p = Pool.objects.get(botstring=pool)
    except Pool.DoesNotExist:
        message.reply('No pool found for {}'.format(pool))
        return

    if msg is None:
        available_messages = BotMessage.objects.filter(endpoint__in=p.endpoints.values_list('id', flat=True)).values_list(
            'botstring',
            flat=True)
        reply = 'Available info: ' + ' '.join(available_messages)
        reply += '\n Try `info {} info_name`'.format(pool)
        message.reply(reply)
        return

    try:
        m = BotMessage.objects.get(botstring=msg, endpoint__in=p.endpoints.values_list('id', flat=True))
    except BotMessage.DoesNotExist:
        message.reply('No bot message found for {}'.format(msg))
        return

    message.reply(m.get_message(p.url, *args.split()))


get_message.botstring = 'info'

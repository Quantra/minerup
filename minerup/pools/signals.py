from django.db.models.signals import post_save
from django.dispatch import receiver

from miners.models import Worker
from miners.tasks import test_worker


@receiver(post_save, sender=Worker)
def worker_post_save_receiver(sender, instance, created, **kwargs):
    """
    Starts a test task for new workers.
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    if created:
        test_worker.delay(instance.id)

# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-19 23:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pools', '0015_auto_20170719_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apifunction',
            name='api_function',
            field=models.CharField(choices=[('test_worker', 'test_worker'), ('hashrate_worker', 'hashrate_worker')], max_length=32),
        ),
    ]

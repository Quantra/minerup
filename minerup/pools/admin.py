from django.contrib import admin

from pools.models import Pool, Coin, ApiEndpoint, BotMessage, ApiFunction


class CoinInline(admin.TabularInline):
    model = Coin
    extra = 0


class BotMessageInline(admin.TabularInline):
    model = BotMessage
    extra = 0


class ApiFunctionInline(admin.TabularInline):
    model = ApiFunction
    extra = 0


class ApiEndpointInline(admin.TabularInline):
    model = Pool.endpoints.through
    fields = ['apiendpoint', 'ep_path']
    readonly_fields = ['ep_path']
    extra = 0

    def ep_path(self, instance):
        return instance.apiendpoint.path


@admin.register(Pool)
class PoolAdmin(admin.ModelAdmin):
    inlines = [ApiEndpointInline]
    exclude = ['endpoints']
    filter_horizontal = ['coins']


admin.site.register(Coin)


@admin.register(ApiEndpoint)
class EndpointAdmin(admin.ModelAdmin):
    inlines = [ApiFunctionInline, BotMessageInline]

admin.site.register(BotMessage)
admin.site.register(ApiFunction)

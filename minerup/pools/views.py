from django.shortcuts import render

from pools.models import BotMessage, Pool, ApiFunction, ApiEndpoint


def test(request):
    p = Pool.objects.get(pk=1)
    m = BotMessage.objects.get(pk=1)
    data = {'message': m.get_message(p.url, '0xadd2b0ab56b4c77e74cd7f0ae0716f4c8f454cd5')}

    print('\n\n\n\n')

    a = ApiFunction.objects.get(pk=1)
    a.exec_function(p.url, 'my_wallet_address', 'my_email', kwallet='123123')

    p2 = Pool.objects.get(label='Dwarf Expanse')
    print(p2.url)
    ae = ApiEndpoint.objects.get(label='Dwarf')
    json = ae.get_json(p2.url, wallet='061496fe144faa4aebbf565dafc22d4253700bb2', email='mail@example.com')
    print(json)

    return render(request, 'test.html', data)

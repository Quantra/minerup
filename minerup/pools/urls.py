from django.conf.urls import url

from pools.views import test

urlpatterns = [
    url(r'^$', test, name='test'),
]

from __future__ import unicode_literals

from django.apps import AppConfig


class PoolsConfig(AppConfig):
    name = 'pools'

    def ready(self):
        import pools.signals

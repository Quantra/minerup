from slackbot.bot import Bot
import os
import django


def main():
    bot = Bot()
    bot.run()

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "minerup.settings")
    django.setup()
    main()

import re

from slackbot.bot import respond_to

from minerbot.coins import MULTI_COINS, COINS, MultiCoin


# todo bin all this shit off and delete minerbot dir.  Bot plugins can go per app.


@respond_to('^hi$', re.IGNORECASE)
@respond_to('^hello$', re.IGNORECASE)
def hi(message):
    message.reply('Hello miner!')
    message.react('wave')


@respond_to('^account (\S+) (\S+) (\S+)$', re.IGNORECASE)
def account(message, cmd, coin_id, account_id):
    coin_id = coin_id.upper()
    cmd = cmd.lower()

    if account_id is not None:
        if coin_id in MULTI_COINS:
            coin_id = 'MULTI'
        coin = COINS.get(coin_id)
        if coin is None:
            message.reply('Wrong fucking coin mate.')
            return
    else:
        account_id = coin_id
        coin = MultiCoin

    CMD = {
        'stats': lambda: coin.account_stats(account_id),
        'workers': lambda: coin.account_workers(account_id),
        'check': lambda: coin.account_check(account_id)
    }

    command = CMD.get(cmd)
    if command is None:
        message.reply('Wrong fucking command mate.')
        return

    msg = command()
    if msg is None:
        message.reply("Something's not fucking right mate.")
        return

    message.reply('Your fucking account {}...'.format(cmd))
    message.reply(msg)


@respond_to('^account (\S+) (\S+)$', re.IGNORECASE)
def multi_account(message, cmd, account_id):
    return account(message, cmd, 'MULTI', account_id)

from urllib.parse import urljoin

import requests


class Coin:
    url = ''
    id = ''
    apis = {
        'blocks': {
            'path': '/api/blocks',
            'latest_id': None
        },
        'account': {
            'path': '/api/accounts/{}',
            'stats_key': 'stats',
            'workers_key': 'workers'
        }
    }

    @classmethod
    def blocks(cls):
        r = requests.get(urljoin('http://' + cls.url, cls.apis['blocks']['path']))
        return r.json()

    @classmethod
    def account_json(cls, account_id):
        """
        Requests account data from minertopia api and returns the json it gets.  todo cacheing/rate limiting
        :param account_id:
        :return:
        """
        try:
            r = requests.get(urljoin('http://' + cls.url, cls.apis['account']['path'].format(account_id)))
            r.raise_for_status()
            return r.json()
        except requests.HTTPError:
            print('Bad request')
            return {}

    @classmethod
    def account_stats(cls, account_id):
        """
        Returns a message for the bot to spit out with account stats.
        :param account_id:
        :return:
        """

        stats = cls.account_json(account_id).get(cls.apis['account']['stats_key'])
        if not stats:
            return

        msg = ''
        for k, v in stats.items():
            msg += '{}: {}, '.format(k, v)

        return msg

    @classmethod
    def account_workers(cls, account_id):
        """
        Returns a message for the bot to spit out with worker stats.
        :param account_id:
        :return:
        """

        workers = cls.account_json(account_id).get(cls.apis['account']['workers_key'])
        if not workers:
            return

        msg = ''
        for worker, stats in workers.items():
            msg += '*{}*: '.format(worker)
            for k, v in stats.items():
                msg += '{}: {} '.format(k, v)

        return msg


    @classmethod
    def account_check(cls, account_id):
        """
        Returns a message for the bot to spit out with workers up/down.
        :param account_id:
        :return:
        """

        json = cls.account_json(account_id)
        total = int(json.get('workersTotal', 0))
        online = int(json.get('workersOnline', 0))
        offline = int(json.get('workersOffline', 0))

        insult = ' Sort it out dickhead.'

        if total <= 0:
            return 'No workers at all.' + insult

        msg = '*Online:* {} *Offline:* {} *Total:* {}'.format(online, offline, total)

        if online <= 0 or offline >= 1:
            msg += insult
        elif online >= total:
            msg += ' Looks fucking good mate!'

        return msg


class Music(Coin):
    url = 'music.minertopia.org'
    id = 'MUSIC'


class EtheremClassic(Coin):
    url = 'etc.minertopia.org'
    id = 'ETC'


class Ubiq(Coin):
    url = 'ubiqminers.org'
    id = 'UBQ'


class MultiCoin(Coin):
    """
    Parent class for coins in the multipool.
    """

    url = 'pool.minertopia.org'

    apis = {
        'account': {
            'path': '/api/wallet',
            'ex_path': '/api/walletEx',
            'workers_key': 'miners'
        }
    }

    @classmethod
    def account_json(cls, account_id):
        try:
            r = requests.get(urljoin('http://' + cls.url, cls.apis['account']['path']), params={'address': account_id})
            r.raise_for_status()
            return r.json()
        except requests.HTTPError:
            print('Bad request')
            return {}

    @classmethod
    def account_ex_json(cls, account_id):
        try:
            r = requests.get(urljoin('http://' + cls.url, cls.apis['account']['ex_path']), params={'address': account_id})
            r.raise_for_status()
            return r.json()
        except requests.HTTPError:
            print('Bad request')
            return {}

    @classmethod
    def account_stats(cls, account_id):
        """
        Returns a message for the bot to spit out with account stats.
        :param account_id:
        :return:
        """

        stats = cls.account_json(account_id)
        if not stats:
            return

        msg = ''
        for k, v in stats.items():
            msg += '{}: {}, '.format(k, v)

        return msg

    @classmethod
    def account_workers(cls, account_id):
        """
        Returns a message for the bot to spit out with worker stats.
        :param account_id:
        :return:
        """

        workers = cls.account_ex_json(account_id).get(cls.apis['account']['workers_key'])
        if not workers:
            return

        msg = ''
        for worker in workers:
            msg += '*{}*: '.format(worker['ID'])
            for k, v in worker.items():
                msg += '{}: {}, '.format(k, v)

        return msg

    @classmethod
    def account_check(cls, account_id):
        return 'Ethash coins only dickhead.'

MULTI_COINS = ['AMS', 'CAKES', 'DMB', 'ESP', 'INFO', 'INSN', 'LBC', 'MUE', 'RAIN', 'SYS', 'UNIT', 'XCT', 'XRBC']

COINS = {
    'MUSIC': Music,
    'ETC': EtheremClassic,
    'MULTI': MultiCoin,
    'UBQ': Ubiq
}

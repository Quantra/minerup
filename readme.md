# Minerup

Django based all in one hackpad for Minertopia apps/projects and other mining related stuffs.

Beginners and experts alike are welcome to learn, code and contribute.  Please join the Minertopia slack and ask for the devs there before you start.


## Todo/ideas list

### API info messages
- Allow functions for messages
- Fields for endpoint args (int) and kwargs (str) and check
- Better error handling

### Block alerter
- Celery beat scans for new blocks per coin
- Users register for slack im when new block is found

### Universal API alerter
- Users can register an alert with url and field ref, choose > or < and a value to trigger the alert.
- On set up test the api and check the field can be float()
- Alert user when triggered

### Slack account linking
- Users link their slack account with their django account.
- Could then allow users to store their workers/rig info.
- Use this info for short bot commands like `checkmyrigs`.
- Could then develop this into dashboard.